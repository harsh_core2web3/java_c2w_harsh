

  class UnsignedShift {
     public static void main(String[] args) {
        int x = 5;
	System.out.println(x>>>2);
	
	int z = -5;
	System.out.println(z>>>2);

	int a = 0b00000000000000000000000000000001;  // binary value after shifting x = 5
	System.out.println(a);

	int b = 0b11111111111111111111111111111011;   // binary value of x = -5
	System.out.println(b);

	int c = 0b00111111111111111111111111111110;  // binary value after shifting z = -5
	System.out.println(c);
     }
  
  }

    // In unsigned shift operators signed bit of bianry is also shifted
