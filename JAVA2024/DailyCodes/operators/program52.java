


 class Negation {
   public static void main(String[] args) {
       int x = 10;
       System.out.println(~x);
       int z = 0b11111111111111111111111111110101;
       System.out.println(z);
   
   }
 }

   // this internally goes as ---
 //
 //   int 10 = 0b 00000000000000000000000000001010;    since its integer we have written 32 bits
 //   now toggle above binary code 
 //   you wil get 0b11111111111111111111111111110101   which is equal to -11 
