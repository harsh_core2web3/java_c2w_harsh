


class BitwiseDemo1{
	public static void main(String[] args){
		int x = 5;
		int y = 7;
		System.out.println(x ^ y); // 2 
	}
}

/*
  here we are use the XOR trut table.
       true   true   false
       true   false  true 
       false  true   true 
       false  false  false
 */
