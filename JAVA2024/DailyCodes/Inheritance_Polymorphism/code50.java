class Demo{
        void fun(char x,char y){
                System.out.println(x);
                System.out.println("IN FUN");
                System.out.println(y);
        }
        void run(double a, float b){
                System.out.println("IN RUN");
                System.out.println(a);
                System.out.println(b);
        }
        public static void main(String[] args){
                Demo obj=new Demo();
                obj.run('A','B');
        	obj.run(10,20);
		obj.run(10.5,20.5);
		obj.run(10.5f,20.5f);
		obj.run(10.5f,20);
		obj.run(10.5,20.5f);
	
	}
}
