
class OPTIMIZEDbubblesortbyRECURSSION{
		
static 	int n;
static int cnt=0;
	static void bubblesort(int arr[] ,int n){
		       
			if(n==1){
	 			return;
			
			}
			boolean swapped= false;
			      for(int j=0 ; j<n-1; j++){
				cnt++;
		 		if(arr[j]>arr[j+1]){
					
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
	  				     swapped = true;		
	    			       		}
					}
				if ( swapped == false){
					return;
				}
				bubblesort(arr,n-1);
		
	}

public static void main(String[] args){

int arr[]=new int[]{7,4,3,5,6,9};
bubblesort(arr,arr.length);
for(int h=0; h<arr.length ; h++){
	System.out.print(arr[h]+ " ");
}


System.out.println();
System.out.println("Iterations = " +cnt);
}

}
