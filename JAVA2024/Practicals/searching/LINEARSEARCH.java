import java.util.Scanner;
class LINEARSEARCH{
	static int searcharr(int arr[],int search){
		for (int i=0; i<arr.length ; i++){
			if(arr[i]==search)
				return i;
		}
		return -1;
	}
	        public static void main(String[] args){
			int arr[] =new int[7];
			Scanner sc=new Scanner(System.in);

			for (int i=0 ; i<arr.length ; i++){
				arr[i]=sc.nextInt();
			}

			int retval=searcharr(arr,-1);

			if(retval==-1)
				System.out.println("ELEMENT NOT FOUND");
		
			else
				        System.out.println("ELEMENT FOUND AT "+retval );
		}
}
